export enum CatColor {
    ORANGE = 'orange',
    GREY = 'grey',
    BLACK = 'black',
}

export enum Color {
  ORANGE = '#fcc700',
  WHITE = '#E2FCEF',
  BLUE = '#006d8f',
  RED = '#f72f00',
  NIGHTTINT = 0x605e63,
  NIGHTGUI = 0x4d0a68,
}

export enum CatActivity {
  IDLE = 'idle',
  STAND = 'stand',
  WALK = 'walk',
  RUN = 'run',
  SLEEP = 'sleep'
}

export enum CatHealth {
  FIT = 'fit',
  SICK = 'sick',
  TIRED = 'tired',
  SLEEPY = 'sleepy',
}

export enum CatDirection {
  LEFT = 'Left',
  RIGHT = 'Right'
}
